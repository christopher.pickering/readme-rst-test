==========
README.rst
==========

width at 100px
--------------
.. image:: https://www.zookcabins.com/wp-content/uploads/blog-posts/planning-hunting-cabin/beautiful-custom-log-cabin-hunting-lodge.jpg
  :alt: demo image
  :width: 100px


width at 100%
--------------
.. image:: https://www.zookcabins.com/wp-content/uploads/blog-posts/planning-hunting-cabin/beautiful-custom-log-cabin-hunting-lodge.jpg
  :alt: demo image
  :width: 100%


another image at 100px
----------------------

.. image:: https://github.com/Riverside-Healthcare/extract_management/blob/main/images/em2.png?raw=true
  :alt: another image
  :width: 100px

another image at 100%
----------------------

.. image:: https://github.com/Riverside-Healthcare/extract_management/blob/main/images/em2.png?raw=true
  :alt: another image
  :width: 100%
  
